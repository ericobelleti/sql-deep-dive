 
 USE AdventureWorks2017
 
 --Two of the functions allow you to pull columns or expressions from a row before (LAG) or after (LEAD) the current row
 
 SELECT CustomerID, OrderDate, SalesOrderID, 
   LAG(SalesOrderID) OVER(PARTITION BY CustomerID ORDER BY SalesOrderID
   ) AS PrevOrder
FROM Sales.SalesOrderHeader
ORDER BY CustomerID;



--The other two functions allow you to return values from the first row of the partition (FIRST_VALUE)
-- OR last row of the partition (LAST_VALUE). FIRST_VALUE and LAST_VALUE also require framing, so be sure to include the frame when using these functions. 

--


SELECT CustomerID, OrderDate, SalesOrderID, 
   FIRST_VALUE(SalesOrderID) OVER(PARTITION BY CustomerID 
      ORDER BY SalesOrderID
   ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS FirstOrder
FROM Sales.SalesOrderHeader;