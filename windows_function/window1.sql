--  https://www.red-gate.com/simple-talk/databases/sql-server/t-sql-programming-sql-server/introduction-to-t-sql-window-functions/#:~:text=The%20frame%2C%20ROWS%20BETWEEN%20UNBOUNDED,1%20to%204%20are%20used.


-- nums  [1 2 1 2 3 3]
-- letters [a, b, c ,4, 6,s]

-- partitioning by nums  3 partions


-- parts 1
-- nums    letters   sumA
--  1          a      1    unbopunding pres
--  1           c      2    current row
--  1           k      3

--parts2
--- 2           b      2
--- 2           4      4


--parts3
--   3          6       3
---  3           s      6




USE  TSQL2012;
GO

SELECT  *  FROM Sales.EmpOrders;
GO


SELECT [empid], [ordermonth],[val],
SUM(val) OVER(PARTITION BY empid ORDER BY ordermonth
 ROWS BETWEEN CURRENT ROW AND 1 following)
runsum
FROM Sales.EmpOrders;
GO



SELECT [empid], [ordermonth],[val],
SUM(val) OVER(PARTITION BY empid ORDER BY ordermonth
 ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)


 
SELECT [empid], [ordermonth],[val],
SUM(val) OVER(PARTITION BY empid ORDER BY ordermonth
 ROWS BETWEEN 1 PRECEDING AND CURRENT ROW)

runsum
FROM Sales.EmpOrders;
GO

--empid	ordermonth	val	runamount
--1	2006-07-01 00:00:00.000	1614.88	1614.88
--1	2006-08-01 00:00:00.000	5555.90	7170.78
--1	2006-09-01 00:00:00.000	6651.00	13821.78


