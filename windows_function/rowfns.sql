
USE TSQL2012;
GO


IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'Dublicates'))
BEGIN

CREATE TABLE Duplicates(Col1 INT, Col2 CHAR(1));
INSERT INTO Duplicates(Col1, Col2) 
VALUES(1,'A'),(2,'B'),(2,'B'),(2,'B'),
	(3,'C'),(4,'D'),(4,'D'),(5,'E'),
	(5,'E'),(5,'E');
SELECT * FROM Duplicates;

END



USE Adventureworks2017; --Or whichever version you have
GO
SELECT SalesOrderID, OrderDate, CustomerID, 
	ROW_NUMBER() OVER(ORDER BY OrderDate) As RowNum,
	RANK() OVER(ORDER BY OrderDate) As Rnk,
	DENSE_RANK() OVER(ORDER BY OrderDate) As DenseRnk
FROM Sales.SalesOrderHeader
WHERE CustomerID = 11330;



SELECT SP.FirstName, SP.LastName,
	SUM(SOH.TotalDue) AS TotalSales, 
	NTILE(4) OVER(ORDER BY SUM(SOH.TotalDue)) * 1000 AS Bonus
FROM [Sales].[vSalesPerson] SP 
JOIN Sales.SalesOrderHeader SOH 
     ON SP.BusinessEntityID = SOH.SalesPersonID 
WHERE SOH.OrderDate >= '2012-01-01' AND SOH.OrderDate < '2013-01-01'
GROUP BY FirstName, LastName;




