-- Section 12. Data definition

-- CREATE DATABASE

CREATE DATABASE TestDb;
GO

-- TABLES IN YOUR DATABASE


USE TestDb;
SELECT * FROM [INFORMATION_SCHEMA].[TABLES]

---TABLE_CATALOG	TABLE_SCHEMA	TABLE_NAME	TABLE_TYPE


-- DATABASES CREATED


SELECT name, database_id, create_date
  FROM [master].[sys].[databases]

  EXEC sp_databases;


-- DELETE DATABASE

DROP DATABASE IF EXISTS TestDb


-- CREATING SCHEMA
USE TestDb
CREATE SCHEMA persons;
GO
--  ALL SCHEMAS
SELECT * FROM sys.schemas
SELECT * FROM sys.sysusers

SELECT 
    s.name AS schema_name, 
    u.name AS schema_owner
FROM 
    sys.schemas s
INNER JOIN sys.sysusers u ON u.uid = s.principal_id
ORDER BY 
    s.name;


	CREATE TABLE persons.jobs(
    job_id INT PRIMARY KEY IDENTITY,
    customer_id INT NOT NULL,
    description VARCHAR(200),
    created_at DATETIME2 NOT NULL
);

--
SELECT  * FROM [DESKTOP-BU494Q4].[TestDb].[persons].[jobs]


CREATE TABLE dbo.offices
(
    office_id      INT
    PRIMARY KEY IDENTITY, 
    office_name    NVARCHAR(40) NOT NULL, 
    office_address NVARCHAR(255) NOT NULL, 
    phone          VARCHAR(20),
);

INSERT INTO 
    dbo.offices(office_name, office_address)
VALUES
    ('Silicon Valley','400 North 1st Street, San Jose, CA 95130'),
    ('Sacramento','1070 River Dr., Sacramento, CA 95820');





CREATE PROCEDURE usp_get_office_by_id(
    @id INT
) AS
BEGIN
    SELECT 
        * 
    FROM 
        dbo.offices
    WHERE 
        office_id = @id;
END;

EXEC usp_get_office_by_id 2

CREATE SCHEMA sales

SELECT * FROM dbo.offices
-- ALTER SCHEMA

ALTER SCHEMA sales TRANSFER OBJECT::dbo.offices; 




ALTER PROCEDURE usp_get_office_by_id(
    @id INT
) AS
BEGIN
    SELECT 
        * 
    FROM 
        sales.offices
    WHERE 
        office_id = @id;
END;

EXEC usp_get_office_by_id 2

--- DROP SCHEMA 
DROP SCHEMA logistics;


--

CREATE SEQUENCE item_counter
    AS INT
    START WITH 10
    INCREMENT BY 10;