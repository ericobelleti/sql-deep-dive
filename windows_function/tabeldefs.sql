USE [TestDb]
GO

USE [TestDb]
GO

/****** Object:  Sequence [dbo].[item_counter]    Script Date: 10/20/2022 9:05:59 AM ******/
CREATE SEQUENCE [dbo].[item_counter] 
 AS [int]
 START WITH 10
 INCREMENT BY 10
 MINVALUE -2147483648
 MAXVALUE 2147483647
 CACHE 
GO


DECLARE @myid uniqueidentifier = NEWID();  
SELECT CONVERT(CHAR(255), @myid) AS 'char';


SELECT NEWID()

