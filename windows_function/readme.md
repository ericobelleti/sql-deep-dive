
ROWS UNBOUNDED PRECEDING is no Teradata-specific syntax, it's Standard SQL. Together with the ORDER BY it defines the window on which the result is calculated.

Logically a Windowed Aggregate Function is newly calculated for each row within the PARTITION based on all ROWS between a starting row and an ending row.

Starting and ending rows might be fixed or relative to the current row based on the following keywords:

CURRENT ROW, the current row
UNBOUNDED PRECEDING, all rows before the current row -> fixed
UNBOUNDED FOLLOWING, all rows after the current row -> fixed
x PRECEDING, x rows before the current row -> relative
y FOLLOWING, y rows after the current row -> relative
Possible kinds of calculation include:

Both starting and ending row are fixed, the window consists of all rows of a partition, e.g. a Group Sum, i.e. aggregate plus detail rows
One end is fixed, the other relative to current row, the number of rows increases or decreases, e.g. a Running Total, Remaining Sum
Starting and ending row are relative to current row, the number of rows within a window is fixed, e.g. a Moving Average over n rows
So SUM(x) OVER (ORDER BY col ROWS UNBOUNDED PRECEDING) results in a Cumulative Sum or Running Total

11 -> 11
 2 -> 11 +  2                = 13
 3 -> 13 +  3 (or 11+2+3)    = 16
44 -> 16 + 44 (or 11+2+3+44) = 60

https://stackoverflow.com/questions/7053471/understanding-the-differences-between-cube-and-rollup
https://www.w3schools.com/sql/func_sqlserver_coalesce.asp
https://www.sqlservercentral.com/articles/the-difference-between-rollup-and-cube
https://learn.microsoft.com/en-us/sql/t-sql/queries/select-window-transact-sql?view=sql-server-ver16
https://www.databasejournal.com/ms-sql/t-sql-programming-part-12-using-the-rollup-cube-and-grouping-sets-operators/


ROOLUP(year, month, day)
year, month, day
month, day
day
()



ROOLUP(deparment, gender)
-- department, gender  4 * 2  summarize by gender and departmenrt
gender  sumarize by gender
()   all genders all de


dept   gender sum
2022      jan    400
2022      feb     200
2022      *allmonths    600
2021      jan    400
2021     feb     200
2021      *allmonths    600
*allyears     *allmonths    1200






ROOLUP(REJOICE)
REJOICE
()


